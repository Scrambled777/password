namespace Password {
    [GtkTemplate (ui = "/io/gitlab/elescoute/password/ui/pageReglages.ui")]
    public class Reglages : Adw.PreferencesWindow {
        [GtkChild]
        public unowned Adw.ActionRow actionPwned;
        [GtkChild]
        public unowned Adw.ComboRow comboHachage;
        [GtkChild]
        public unowned Gtk.Switch switchConversion;
        [GtkChild]
        public unowned Adw.ActionRow actionSalt;
        [GtkChild]
        public unowned Gtk.Entry texteSalage;
        [GtkChild]
        public unowned Adw.ActionRow actionAlias;
        [GtkChild]
        public unowned Gtk.Entry preAlias;
        [GtkChild]
        public unowned Gtk.Switch switchMasqueAlias;
        [GtkChild]
        public unowned Adw.ActionRow actionLength;
        [GtkChild]
        public unowned Gtk.SpinButton scaleLongueur;
        [GtkChild]
        public unowned Gtk.Switch switchMinuscules;
        [GtkChild]
        public unowned Gtk.Switch switchMajuscules;
        [GtkChild]
        public unowned Gtk.Switch switchChiffres;
        [GtkChild]
        public unowned Gtk.Switch switchPin;
        [GtkChild]
        public unowned Gtk.Switch switchSpeciaux;
        [GtkChild]
        public unowned Gtk.Switch switchEnablePwned;
        [GtkChild]
        public unowned Gtk.Switch switchPwned;
        [GtkChild]
        public unowned Adw.ComboRow comboMonitor;
        [GtkChild]
        public unowned Gtk.Switch switchEmbed;
        [GtkChild]
        public unowned Gtk.Switch switchTab;
        [GtkChild]
        public unowned Gtk.Switch switchColor;
        [GtkChild]
        public unowned Gtk.Switch switchQuitter;
        [GtkChild]
        public unowned Gtk.Switch switchSuppression;
        [GtkChild]
        public unowned Gtk.SpinButton scaleTemps;
        [GtkChild]
        public unowned Gtk.Adjustment temps;
        [GtkChild]
        public unowned Gtk.Switch switchNotifications;
        [GtkChild]
        public unowned Gtk.Button boutonSupprimerProfil;
        [GtkChild]
        public unowned Adw.ActionRow actionSupProfil;
        [GtkChild]
        public unowned Gtk.Switch switchVerrouillage;
        [GtkChild]
        public unowned Gtk.Label labelForce;
        [GtkChild]
        public unowned Gtk.Image iconeBouclier;
        [GtkChild]
        public unowned Gtk.LevelBar barreForce;
        [GtkChild]
        public unowned Gtk.Label label_strength;

        public Window win {get; set;}

        public Widgets.Menu menu {get;set;}

        public Calculateur calculateur {get;set;}

        public Monitor monitor {get;set;}

        public Reglages (Window win, Widgets.Menu menu, Calculateur calculateur, Monitor monitor) {
            Object (
                win: win,
                menu: menu,
                calculateur: calculateur,
                monitor: monitor
            );

            transient_for = win;
            show ();

            if(dev) message("preferences");

            texteSalage.icon_press.connect(() => {texteSalage.set_text("");});
            preAlias.icon_press.connect(() => {preAlias.set_text("");});
            preAlias.changed.connect(preAliasChanged);
            switchMasqueAlias.notify["active"].connect(switchMasqueAliasActive);

            comboHachage.notify["selected"].connect(() => {parametres[0] = (int32) comboHachage.get_selected(); chgtReglages();});
            switchConversion.notify["active"].connect(() => {parametres[1] = switchConversion.get_active(); chgtReglages();});
            texteSalage.changed.connect(() => {parametres[2] = texteSalage.get_text(); chgtReglages();});
            preAlias.changed.connect(() => {parametres[3] = preAlias.get_text(); chgtReglages();});
            scaleLongueur.value_changed.connect(() => {parametres[4] = (int) scaleLongueur.get_value(); chgtReglages();});
            switchMinuscules.notify["active"].connect(basculSwitchMinuscules);
            switchMajuscules.notify["active"].connect(basculSwitchMajuscules);
            switchChiffres.notify["active"].connect(basculSwitchChiffres);
            switchPin.notify["active"].connect(basculSwitchPin);
            switchSpeciaux.notify["active"].connect(() => {parametres[9] = switchSpeciaux.get_active(); chgtReglages();});

            switchNotifications.notify["active"].connect(() => {if(switchNotifications.get_active()) {avertissement("notifications");}});
            switchSuppression.notify["active"].connect(() => {if(switchSuppression.get_active()) {avertissement("clipboard");}});

            switchEnablePwned.notify["active"].connect(switchEnablePwnedActive);
            switchPwned.notify["active"].connect(switchPwnedActive);

            forceMotDePasse(this);
            if(win.profilActuel=="") actionSupProfil.hide();
            if(win.profilActuel!="") {
                actionSupProfil.set_title(_("Delete the profile") + " <b>" + win.profilActuel + "</b>");
                boutonSupprimerProfil.clicked.connect(suppressionProfil);
            }

            if(win.codeVerrouillage!="") switchVerrouillage.set_active(true);

            switchVerrouillage.notify["active"].connect(switchVerrouillageActive);

            comboMonitor.notify["selected"].connect(chgtMonitor);

            barreForce.add_offset_value ("very-weak", 1);
            barreForce.add_offset_value ("weak", 2);
            barreForce.add_offset_value ("fairly-strong", 3);
            barreForce.add_offset_value ("strong", 4);
            barreForce.add_offset_value ("very-strong", 5);

            this.close_request.connect(fermeture);
        }

        construct {
            comboHachage.set_selected((uint) parametres[0].get_int32());
            switchConversion.set_active(parametres[1].get_boolean());
            texteSalage.set_text(parametres[2].get_string());
            if(parametres[2].get_string()!="") texteSalage.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            preAlias.set_text(parametres[3].get_string());
            if(parametres[3].get_string()!="") preAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            scaleLongueur.set_value(parametres[4].get_int32());
            switchMinuscules.set_active(parametres[5].get_boolean());
            switchMajuscules.set_active(parametres[6].get_boolean());
            switchChiffres.set_active(parametres[7].get_boolean());
            switchPin.set_active(parametres[8].get_boolean());
            switchSpeciaux.set_active(parametres[9].get_boolean());

            settings.bind("masquagealias", switchMasqueAlias, "active", DEFAULT);
            settings.bind("enablepwn", switchEnablePwned, "active", DEFAULT);
            settings.bind("pwn", switchPwned, "active", DEFAULT);
            settings.bind("moniteur", comboMonitor, "selected", DEFAULT);
            settings.bind("embed", switchEmbed, "active", DEFAULT);
            settings.bind("session", switchTab, "active", DEFAULT);
            settings.bind("couleur", switchColor, "active", DEFAULT);
            settings.bind("quitter", switchQuitter, "active", DEFAULT);
            settings.bind("suppression", switchSuppression, "active", DEFAULT);
            settings.bind("delai", temps, "value", DEFAULT);
            settings.bind("notifications", switchNotifications, "active", DEFAULT);

            comboHachage.set_subtitle_lines (1);
            actionSalt.set_subtitle_lines (1);
            actionAlias.set_subtitle_lines (1);
            actionLength.set_subtitle_lines (1);
            texteSalage.set_size_request (100, -1);
            preAlias.set_size_request (100, -1);

            actionPwned.set_sensitive(false);

            if(settings.get_boolean("enablepwn")) actionPwned.set_sensitive(true);

        }

        private void basculSwitchMinuscules() {
            message("minus");
            parametres[5] = switchMinuscules.get_active();
            if(switchMinuscules.get_state() && switchMajuscules.get_state()){
                switchMajuscules.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void basculSwitchMajuscules() {
            parametres[6] = switchMajuscules.get_active();
            if(switchMajuscules.get_state() && switchMinuscules.get_state()){
                switchMinuscules.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void basculSwitchChiffres() {
            parametres[7] = switchChiffres.get_active();
            if(switchChiffres.get_state() && switchPin.get_state()){
                switchPin.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void basculSwitchPin() {
            parametres[8] = switchPin.get_active();
            if(switchPin.get_state() && switchChiffres.get_state()){
                switchChiffres.set_active(false);
            }
            else{
                chgtReglages();
            }
        }

        private void preAliasChanged() {
            calculateur.texteAlias.set_text(preAlias.get_text());
        }

        private void switchMasqueAliasActive() {
            calculateur.texteAlias.set_visibility(!switchMasqueAlias.get_active());
        }

        private void switchEnablePwnedActive() {
            actionPwned.set_sensitive(switchEnablePwned.get_active());
            win.boutonPwned.set_visible(!switchPwned.get_active() && switchEnablePwned.get_active());
        }

        private void switchPwnedActive() {
            win.boutonPwned.set_visible(!switchPwned.get_active() && switchEnablePwned.get_active());
        }

        private void chgtReglages() {
            if(win.codeVerrouillage == "") ecritureFichierConfig(win.profilActuel, win.dossierConfig);
            if(win.codeVerrouillage != "") ecritureFichierCryptConfig(win.profilActuel, win.dossierConfig, win.codeVerrouillage);

            if(preAlias.get_text()!="" && preAlias.get_icon_name(SECONDARY)==null) preAlias.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            if(preAlias.get_text()=="" && preAlias.get_icon_name(SECONDARY)!=null) preAlias.set_icon_from_icon_name(SECONDARY,null);
            if(texteSalage.get_text()!="" && texteSalage.get_icon_name(SECONDARY)==null) texteSalage.set_icon_from_icon_name(SECONDARY,"edit-delete-symbolic");
            if(texteSalage.get_text()=="" && texteSalage.get_icon_name(SECONDARY)!=null) texteSalage.set_icon_from_icon_name(SECONDARY,null);

            forceMotDePasse(this);
        }

        private void avertissement(string param) {
            string fichier ="/.initWarn";
            if(param=="notifications") fichier+="Notif";
            if(param=="clipboard") fichier+="Clip";
            string message =_("This feature may not work on your system");
            if(!FileUtils.test(win.dossierConfig+fichier, FileTest.EXISTS)){
                Adw.MessageDialog dialog = new Adw.MessageDialog(this, message, "");
                dialog.add_response("ok", _("OK"));
                dialog.present();
                GLib.FileUtils.set_contents(win.dossierConfig+fichier, "");
            }
        }

        private void suppressionProfil() {
            //AVERTISSEMENT
            Adw.MessageDialog dialog = new Adw.MessageDialog(this, _("Are you sure you want to delete the profile "), win.profilActuel);
            dialog.close_response = "cancel";
            dialog.add_response("cancel", _("Cancel"));
            dialog.add_response("delete", _("Delete"));
            dialog.set_response_appearance("delete", Adw.ResponseAppearance.DESTRUCTIVE);

            dialog.response.connect( (response) => {
                if (response == "delete"){
                    FileUtils.remove(win.dossierConfig + "/password-" + win.profilActuel + ".conf");
                    menu.majListeProfils(false);
                    //ON CHARGE LE PREMIER PROFIL DE LA LISTE SI LISTE NON VIDE"
                    if(win.nbProfils==0) {
                        menu.chargementProfil("");
                        FileUtils.remove (win.dossierConfig + "/profile.conf");
                    }
                    if(win.nbProfils >0) menu.chargementProfil(win.listeProfils.nth_data(0));
                    //SUPPRESSION FICHIER DERNIER PROFIL CHARGÉ
                    this.close();
                }
            });

            dialog.present();
        }

        private void switchVerrouillageActive() {
            if(switchVerrouillage.get_active()) fenetreVerrouillage();
            else deverrouillage();
        }

        private void fenetreVerrouillage() {
            new DialogVerrouillage(this);
        }

        public void verrouillage(string code) {

            win.codeVerrouillage = code;
            ecritureCle(win.dossierConfig, win.cle1, win.cle2, win.codeVerrouillage, win.uuid);
            ecritureFichierCryptConfig(win.profilActuel, win.dossierConfig, win.codeVerrouillage);//REECRITURE FICHIER CONFIG CHIFFRÉ
            if(win.listeProfils.length() > 0) chiffrementProfils(win.listeProfils,win.profilActuel,win.dossierConfig,win.codeVerrouillage);

            win.iconeLock.set_from_icon_name("lock-symbolic");
            win.iconeLock.set_tooltip_text(_("Your settings are encrypted"));
        }

        private void deverrouillage() {

            if(win.listeProfils.length() > 0) dechiffrementProfils(win.listeProfils,win.profilActuel,win.dossierConfig,win.codeVerrouillage);
            win.codeVerrouillage = "";
            FileUtils.remove(win.dossierConfig + "/cryptKey.conf");
            ecritureFichierConfig(win.profilActuel, win.dossierConfig);//REECRITURE FICHIER CONFIG EN CLAIR

            win.iconeLock.set_from_icon_name("unlock-symbolic");
            win.iconeLock.set_tooltip_text(_("Your settings are not encrypted"));
        }

        private void chgtMonitor() {

            if(win.stack.get_visible_child_name()=="monitor" && monitor.stackMonitor.get_visible_child_name()=="pageWeb") {
                string url = "https://haveibeenpwned.com/";
                if((int32) comboMonitor.get_selected() == 0) url = "https://monitor.mozilla.org/";
                monitor.webView.load_uri (url);
            }
        }

        private bool fermeture() {
            win.chgtReglages();
            return false;
        }
    }
}

