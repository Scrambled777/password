namespace Password {
    [GtkTemplate (ui = "/io/gitlab/elescoute/password/ui/pageMonitor.ui")]
    public class Monitor : Gtk.Box {
        [GtkChild]
        public unowned Adw.PasswordEntryRow passPwned;
        [GtkChild]
        public unowned Gtk.Button boutonPwned2;
        [GtkChild]
        public unowned Gtk.Label labelFaillePwned;
        [GtkChild]
        public unowned Gtk.Label labelFaillePwned2;
        [GtkChild]
        public unowned Gtk.Button boutonMonitor;
        [GtkChild]
        public unowned Gtk.Viewport view;
        [GtkChild]
        public unowned Gtk.Stack stackMonitor;
        [GtkChild]
        public unowned Gtk.ScrolledWindow pageMonitor;
        [GtkChild]
        public unowned Gtk.ScrolledWindow pageWeb;

        public WebKit.WebView webView = new WebKit.WebView ();

        public Window win {get; set;}

        public Monitor (Window win) {
            Object (
                win: win
            );

            view.set_child(this.webView);
            pageWeb.set_policy(NEVER, AUTOMATIC);

            //passPwned.icon_press.connect(passPwnedChgt);
            boutonPwned2.clicked.connect(majFaillePwned);
            passPwned.entry_activated.connect(majFaillePwned);
            passPwned.changed.connect(() => {labelFaillePwned.set_text("");labelFaillePwned2.hide();});
            boutonMonitor.clicked.connect(lancementMonitor);
            win.boutonRetour.clicked.connect(boutonRetourClicked);
        }

        construct {
            unichar ch ='●';
            Pango.AttrList attrsEntrees = new Pango.AttrList ();
            attrsEntrees.insert (Pango.attr_family_new ("Monospace"));
            //passPwned.set_attributes(attrsEntrees);
            //passPwned.set_invisible_char(ch);
            //passPwned.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
            labelFaillePwned2.hide();
        }

        /*public void passPwnedChgt() {

            if(passPwned.get_icon_name(SECONDARY)=="view-conceal-symbolic"){
                passPwned.set_icon_from_icon_name(SECONDARY,"view-reveal-symbolic");
                passPwned.set_visibility(false);
            }
            else{
                passPwned.set_icon_from_icon_name(SECONDARY,"view-conceal-symbolic");
                passPwned.set_visibility(true);
            }
        }*/

        public void majFaillePwned()
        {
             labelFaillePwned.set_label("");
             labelFaillePwned2.hide();
             if(passPwned.get_text()!=""){
                 int faille = appelCheckPwned(passPwned.get_text());
                 if(faille > 0){
                    labelFaillePwned.set_use_markup(true);
                    labelFaillePwned.set_label("<span foreground=\"#ED0000\" weight=\"bold\">" + _("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database") + "</span>");
                    labelFaillePwned2.show();
                }
                else if(faille==0){
                    labelFaillePwned.set_use_markup(true);
                    labelFaillePwned.set_label("<span foreground=\"#00BC14\" weight=\"bold\">" + _("This password does not appear in HaveIBeenPwned database") + "</span>");
                }
                else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                    labelFaillePwned.set_use_markup(true);
                    labelFaillePwned.set_label("<span foreground=\"#ED0000\" weight=\"bold\">" + _("Check your internet connection!") + "</span>");
                 }
             }
        }

        private void lancementMonitor() {

            string url = "https://haveibeenpwned.com/";
            if(settings.get_int("moniteur")==0) url = "https://monitor.mozilla.org/";

            if(settings.get_boolean("embed")){
                if(win.phosh) win.revealHaut.set_reveal_child(true);
                if(!win.phosh) win.revealBarre.set_reveal_child(true);
                stackMonitor.set_visible_child_name("pageWeb");
                webView.load_uri (url);
            }
            else {
                Gtk.UriLauncher launcher = new Gtk.UriLauncher (url);
                launcher.launch.begin (win, null, (obj, res) => {
                    try {
                        launcher.launch.end (res);
                    } catch (Error error) {
                    //warning ("Could not open help: %s", error.message);
                    }
                });
            }
        }

        private void boutonRetourClicked() {
            if(!webView.can_go_back()){
                stackMonitor.set_visible_child_name("pageMonitor");
                if(win.revealHaut.get_child_revealed())win.revealHaut.set_reveal_child(false);
                if(win.revealBarre.get_child_revealed())win.revealBarre.set_reveal_child(false);
            }
            else webView.go_back();
        }

    }
}

