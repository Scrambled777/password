namespace Password {
    [GtkTemplate (ui = "/io/gitlab/elescoute/password/ui/pageGenerateur.ui")]
    public class Generateur : Gtk.Box {
        [GtkChild]
        public unowned Gtk.Button boutonAleatoire;
        [GtkChild]
        public unowned Gtk.Label motDePasseAleatoire;
        [GtkChild]
        public unowned Gtk.Label labelFailleAleatoire;
        [GtkChild]
        public unowned Gtk.Label labelProfil;

        public Window win {get; set;}

        public Generateur (Window win) {
            Object (
                win: win
            );

            boutonAleatoire.clicked.connect(generation);
        }

        construct {
            motDePasseAleatoire.set_label("");
        }

        public void generation() {

            win.boutonNettoyer.set_icon_name("user-trash-full-symbolic");
            string[] mdp = mdpAleatoire(true, parametres[4].get_int32(), settings.get_boolean("masquage"),//
                parametres[5].get_boolean(), parametres[6].get_boolean(), parametres[7].get_boolean(),//
                parametres[9].get_boolean(), parametres[8].get_boolean(), win.randomPassword );
            motDePasseAleatoire.set_label(couleur(mdp[2]));
            win.pass = mdp[1];
            win.randomPassword = mdp[0];
            win.revealCopier.set_reveal_child(true);
            if(settings.get_boolean("enablepwn") &&  settings.get_boolean("pwn")) GLib.Timeout.add (100, () => {majFailleAleatoire();return false;});
            else{labelFailleAleatoire.set_label("");}
        }

        public void majFailleAleatoire() {

            //HAVEIBEENPWNED
            int faille = appelCheckPwned(win.pass);
            if(faille > 0){
                labelFailleAleatoire.set_use_markup(true);
                labelFailleAleatoire.set_label("<span foreground=\"#ED0000\" weight=\"bold\">" + _("Be careful!\nThis password appears ")+ faille.to_string() + _(" times in HaveIBeenPwned database") + "</span>");
            }
            else if(faille == 0){
                labelFailleAleatoire.set_use_markup(true);
                labelFailleAleatoire.set_label("<span foreground=\"#00BC14\" weight=\"bold\">" + _("This password does not appear in HaveIBeenPwned database") + "</span>");
            }
            else{//FAILLE = -1 : PAS DE CONNEXION INTERNET
                labelFailleAleatoire.set_use_markup(true);
                labelFailleAleatoire.set_label("<span foreground=\"#ED0000\" weight=\"bold\">" + _("Check your internet connection!") + "</span>");
            }
        }

    }
}


