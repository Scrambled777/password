namespace Password{

    public int calculForce(int length, bool remLower, bool remUpper, bool remNum, bool remSpec, bool onlyPin){

        int force = 0;
        int N=0;
        if(!remLower) N += 26;
        if(!remUpper) N += 26;
        if(!remNum) N += 10;
        if(!remSpec) N += 3;
        if(onlyPin) N = 10;

        for (int i=0;i <=128; i++) {
            if(Math.pow(2,i) < Math.pow(N,length)) force=i;
        }

        return force;
    }

    public void forceMotDePasse(Password.Reglages reglages)
        {
            int force = calculForce(parametres[4].get_int32(), parametres[5].get_boolean(), parametres[6].get_boolean(), parametres[7].get_boolean(), parametres[9].get_boolean(), parametres[8].get_boolean());

            int niveauBarre = 0;

            if(force < 64){
                niveauBarre = 1;
                reglages.iconeBouclier.set_from_icon_name("bouclier_0");
                reglages.labelForce.set_text(_("Very weak password") + " [" + force.to_string() + " bits]");
            }
            else if(force < 80){
                niveauBarre = 2;
                reglages.iconeBouclier.set_from_icon_name("bouclier_1");
                reglages.labelForce.set_text(_("Weak password") + " [" + force.to_string() + " bits]");
            }
            else if(force < 100){
                niveauBarre = 3;
                reglages.iconeBouclier.set_from_icon_name("bouclier_2");
                reglages.labelForce.set_text(_("Fairly strong password") + " [" + force.to_string() + " bits]");
            }
            else if(force < 128){
                niveauBarre = 4;
                reglages.iconeBouclier.set_from_icon_name("bouclier_3");
                reglages.labelForce.set_text(_("Strong password") + " [" + force.to_string() + " bits]");
            }
            else{
                niveauBarre = 5;
                reglages.iconeBouclier.set_from_icon_name("bouclier_4");
                reglages.labelForce.set_text(_("Very strong password") + " [≥ 128 bits]");
            }
            reglages.barreForce.value = niveauBarre;
            reglages.iconeBouclier.set_icon_size(LARGE);
        }
}
