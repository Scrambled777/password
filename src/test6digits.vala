namespace Password {

    public bool test6digits(string code_1, string code_2) {
        bool test = false;
        if(code_1.char_count() == 6 && code_2.char_count() == 6 && code_1 == code_2){
            bool testNum = true;
            //VERIFICATION QUE CODE1 EST COMPOSÉ UNIQUEMENT DE CHIFFRE
            for(int i=0;i<6;i++){
                if(!code_1.get_char(i).isdigit() && testNum) testNum = false;
            }
            if(testNum) test = true;
        }
        return test;
    }
}
