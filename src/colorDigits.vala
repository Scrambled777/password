namespace Password {

    public string couleur(string mdp) {

        string mdpCouleur = "<span font_desc=\"Liberation Mono 12\">";
        if(settings.get_boolean("couleur")){
            for(int i=0;i<mdp.length;i++){
                if(mdp.get_char(i).isdigit()) mdpCouleur += "<span foreground=\"green\" weight=\"bold\">" + mdp.substring(i,1) + "</span>";
                else mdpCouleur += mdp.substring(i,1);
            }

        }
        else{
            mdpCouleur += mdp;
        }

        mdpCouleur+="</span>";

        return mdpCouleur;
    }
}
