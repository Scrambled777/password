namespace Password{

    public string[] mdpAleatoire(bool generate, int length, bool mask, bool remLower, bool remUpper, bool remNum, bool remSpec, bool onlyPin, string randomPassword) {

        string[] sortie = {null};
        string alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz*-+=$@:;.,!?~#|";//<>/&";
        if(generate){
            char s[128];
            for (int i = 0; i < 128; ++i) {
                s[i] = alphanum[Random.int_range(0,alphanum.length - 1)];
            }

            s[128] = 0;

            randomPassword = (string) s;//.to_string (); //MOT DE PASSE LONG A RE-MODIFIER
        }

        sortie[0] = randomPassword;

        string pass = randomPassword.substring(0,length);

        //REMOVE NUMERIC

        if(remNum) {
            pass = pass.replace("0", "+");
            pass = pass.replace("1", "/");
            pass = pass.replace("2", "=");
            pass = pass.replace("3", "A");
            pass = pass.replace("4", "B");
            pass = pass.replace("5", "C");
            pass = pass.replace("6", "D");
            pass = pass.replace("7", "E");
            pass = pass.replace("8", "F");
            pass = pass.replace("9", "G");
        }

        //REMOVE SPECIAL

        if(!onlyPin && remSpec) {
            pass = pass.replace("+", "A");
            pass = pass.replace("/", "B");
            pass = pass.replace("*", "C");
            pass = pass.replace("-", "D");
            pass = pass.replace("/", "E");
            pass = pass.replace("=", "F");
            pass = pass.replace("$", "G");
            pass = pass.replace("@", "H");
            pass = pass.replace(":", "I");
            pass = pass.replace(";", "J");
            pass = pass.replace(".", "K");
            pass = pass.replace(",", "L");
            pass = pass.replace("!", "M");
            pass = pass.replace("?", "N");
            pass = pass.replace("~", "O");
            pass = pass.replace("&", "P");
            pass = pass.replace("#", "Q");
            pass = pass.replace("|", "R");
            pass = pass.replace("<", "S");
            pass = pass.replace(">", "T");
        }

        //REMOVE LOWER

        if(!onlyPin && remLower) pass = pass.ascii_down();

        //REMOVE UPPER

        if(!onlyPin && remUpper) pass = pass.ascii_up();

        if(onlyPin) {
            pass = pass.ascii_up();
            pass = pass.replace("+", "0");
            pass = pass.replace("/", "1");
            pass = pass.replace("=", "2");
            pass = pass.replace("*", "3");
            pass = pass.replace("-", "4");
            pass = pass.replace("/", "5");
            pass = pass.replace("$", "6");
            pass = pass.replace("@", "7");
            pass = pass.replace(":", "8");
            pass = pass.replace(";", "9");
            pass = pass.replace(".", "0");
            pass = pass.replace(",", "1");
            pass = pass.replace("!", "2");
            pass = pass.replace("?", "3");
            pass = pass.replace("~", "4");
            pass = pass.replace("&", "5");
            pass = pass.replace("#", "6");
            pass = pass.replace("|", "7");
            pass = pass.replace("<", "8");
            pass = pass.replace(">", "9");
            pass = pass.replace("A", "0");
            pass = pass.replace("B", "1");
            pass = pass.replace("C", "2");
            pass = pass.replace("D", "3");
            pass = pass.replace("E", "4");
            pass = pass.replace("F", "5");
            pass = pass.replace("G", "6");
            pass = pass.replace("H", "7");
            pass = pass.replace("I", "8");
            pass = pass.replace("J", "9");
            pass = pass.replace("K", "0");
            pass = pass.replace("L", "1");
            pass = pass.replace("M", "2");
            pass = pass.replace("N", "3");
            pass = pass.replace("O", "4");
            pass = pass.replace("P", "5");
            pass = pass.replace("Q", "6");
            pass = pass.replace("R", "7");
            pass = pass.replace("S", "8");
            pass = pass.replace("T", "9");
            pass = pass.replace("U", "0");
            pass = pass.replace("V", "1");
            pass = pass.replace("W", "2");
            pass = pass.replace("X", "3");
            pass = pass.replace("Y", "4");
            pass = pass.replace("Z", "5");
        }

        //MASQUAGE

        string randomPasswordMask;
        randomPasswordMask = "";

        int nbMask=1;
        if(length>7) nbMask=2;
        if(length>10) nbMask=3;
        for(int i=0; i<length; i++){

            if(i<nbMask || i>=length-nbMask){
                randomPasswordMask += pass.substring(i,1);
            }
            else{
                randomPasswordMask += "\u25CF";
            }
        }

        // sortie[0] : randomPassword => mot de passe long non modifié
        // sortie[1] : pass => mot de passe final
        // sortie[2] : pass ou randomPasswordMask => mot de passe à afficher

        sortie += pass;

        if(!mask) sortie += randomPasswordMask;
        if(mask) sortie += pass;

        return sortie;

    }
}
