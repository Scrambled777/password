namespace Password {

    public class DialogVerrouillage : Adw.MessageDialog {

        public Reglages reglages {get; set;}

        private Adw.PasswordEntryRow entreeCode_1;
        private Adw.PasswordEntryRow entreeCode_2;
        private Gtk.ProgressBar barre;
        private Gtk.Button bouton;
        private Gtk.Image icone;

        public DialogVerrouillage (Reglages reglages) {
            Object (
                reglages: reglages
            );

            transient_for = reglages;

            set_heading(_("Lock settings"));
            set_body(_("Enter a 6 digits code\nThis code will be needed to access settings page"));

            close_response = "cancel";
            add_response("cancel", _("Cancel"));
            add_response("lock", _("Lock"));
            set_response_appearance("lock", Adw.ResponseAppearance.SUGGESTED);
            set_response_enabled ("lock", false);

            Gtk.Box boite = new Gtk.Box(Gtk.Orientation.VERTICAL,3);

            Adw.PreferencesGroup groupe = new Adw.PreferencesGroup();
            entreeCode_1 = new Adw.PasswordEntryRow();
            entreeCode_2 = new Adw.PasswordEntryRow();

            entreeCode_1.set_alignment((float) 0.5);
            entreeCode_2.set_alignment((float) 0.5);
            entreeCode_1.set_title ("Enter pin code");
            entreeCode_2.set_title ("Confirm pin code");

            bouton = new Gtk.Button();
            bouton.set_label(_("Generate a random pin code..."));
            bouton.add_css_class("suggested-action");
            bouton.set_margin_start(20);
            bouton.set_margin_end(20);

            barre = new Gtk.ProgressBar();
            barre.set_margin_top(-5);
            barre.set_margin_start(25);
            barre.set_margin_end(25);
            barre.set_visible(false);

            icone = new Gtk.Image();
            icone.set_margin_top(15);
            icone.set_from_icon_name("dialog-warning-symbolic");
            Gtk.Label labelInfoLock = new Gtk.Label(_("This program uses the username, hostname and machine-id to generate a unique id to encrypt key and settings files. If one of these parameters is changed, the lock code will be unreadable and settings will need to be reset."));
            labelInfoLock.set_wrap(true);

            Pango.AttrList attrs = new Pango.AttrList ();
            attrs.insert (Pango.attr_weight_new (LIGHT));
            attrs.insert (Pango.attr_scale_new (0.8));
            labelInfoLock.set_attributes(attrs);
            labelInfoLock.set_hexpand(false);
            labelInfoLock.set_margin_top(5);

            groupe.add(entreeCode_1);
            groupe.add(entreeCode_2);
            boite.append(groupe);

            groupe.set_margin_bottom(5);

            boite.append(bouton);
            boite.append(barre);
            boite.append(icone);
            boite.append(labelInfoLock);

            set_extra_child(boite);

            //CONNEXIONS
            entreeCode_1.entry_activated.connect(entreeCode1Activated);
            entreeCode_1.changed.connect(entreeCode1Changed);
            bouton.clicked.connect(boutonClicked);
            entreeCode_2.changed.connect(entreeCode2Changed);
            entreeCode_2.entry_activated.connect(entreeCode2Activated);

            response.connect((response) => {fermeture(response);});

            present ();
        }

        construct {

        }

        private void fermeture(string response) {
            if(response=="lock") {
                reglages.verrouillage(entreeCode_1.get_text());
            }
            else {
                reglages.switchVerrouillage.set_active(false);
            }
        }

        private void entreeCode1Activated() {
            entreeCode_2.grab_focus();
        }

        private void entreeCode1Changed() {
            if(entreeCode_1.get_text().length > 6) {
                GLib.Timeout.add (0, () => {
                    entreeCode_1.set_text(entreeCode_1.get_text().substring(0,6));
                    entreeCode_1.set_position(6);
                    return false;
                });
            }
            if(entreeCode_1.get_text()!="" && entreeCode_2.get_text()!="") {
                if(test6digits(entreeCode_1.get_text(),entreeCode_2.get_text())) set_response_enabled ("lock", true);
                else set_response_enabled ("lock", false);
            }
        }

        private void boutonClicked() {
            if(!barre.is_visible()){
                entreeCode_1.set_text(mdpAleatoire(true, 6, true,false,false,false,false,true,"")[1]);
                entreeCode_2.set_text(entreeCode_1.get_text());
                bouton.set_label(entreeCode_1.get_text());
                barre.set_visible(true);
                icone.set_margin_top(4);
                GLib.Timeout.add (5000, () => {
                    bouton.set_label(_("Generate a random pin code..."));
                    barre.set_visible(false);
                    icone.set_margin_top(15);
                    return false;
                });
                for(int i=0;i<500;i++) {
                    double niveau = (double) i/500;
                    int temps = i*10;
                    GLib.Timeout.add(temps, () => {barre.set_fraction(niveau);return false;});
                }
            }
        }

        private void entreeCode2Changed() {
            if(entreeCode_2.get_text().length > 6) {
                   GLib.Timeout.add (0, () => {
                    entreeCode_2.set_text(entreeCode_2.get_text().substring(0,6));
                    entreeCode_2.set_position(6);
                    return false;
                });
            }
            if(entreeCode_1.get_text()!="" && entreeCode_2.get_text()!="") {
                if(test6digits(entreeCode_1.get_text(),entreeCode_2.get_text())) set_response_enabled ("lock", true);
                else set_response_enabled ("lock", false);
            }
        }

        private void entreeCode2Activated() {
            if(get_response_enabled("lock")) {
                reglages.verrouillage(entreeCode_1.get_text());
                close_response = "lock";
                close();
            }
        }
    }
}
