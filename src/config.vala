namespace Password{

    public void lectureFichierConfig(string dossierConfig, string profilActuel) {

        if(profilActuel=="") {
            string fichierConfig = dossierConfig + "/password.conf";
            var dis = FileStream.open(fichierConfig, "r");
            parametres[2]=dis.read_line();
            parametres[3]=dis.read_line();
            parametres[0]=(int.parse(dis.read_line()));
            parametres[1]=(bool.parse(dis.read_line()));
            dis.read_line();//QUIT (REMOVE FROM 1.5.0)
            parametres[4]=(int.parse(dis.read_line()));
            parametres[5]=(bool.parse(dis.read_line()));
            parametres[6]=(bool.parse(dis.read_line()));
            parametres[7]=(bool.parse(dis.read_line()));
            parametres[9]=(bool.parse(dis.read_line()));
            dis.read_line();//SUPPRESSION (REMOVE FROM 1.5.0)
            dis.read_line();//TIME (REMOVE FROM 1.5.0)
            dis.read_line();//NOTIF (REMOVE FROM 1.5.0)
            parametres[8]=(bool.parse(dis.read_line()));
        }
        else {
            string fichierConfigProfil = dossierConfig + "/password-" + profilActuel + ".conf";
            var dis = FileStream.open(fichierConfigProfil, "r");
            parametres[2]=dis.read_line();
            parametres[3]=dis.read_line();
            parametres[0]=(int.parse(dis.read_line()));
            parametres[1]=(bool.parse(dis.read_line()));
            parametres[4]=(int.parse(dis.read_line()));
            parametres[5]=(bool.parse(dis.read_line()));
            parametres[6]=(bool.parse(dis.read_line()));
            parametres[7]=(bool.parse(dis.read_line()));
            parametres[9]=(bool.parse(dis.read_line()));
            parametres[8]=(bool.parse(dis.read_line()));
        }
    }

    public void lectureFichierCryptConfig(string dossierConfig, string profilActuel, string codeVerrouillage) {

        string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true)[0];

        if(profilActuel==""){
            string fichierConfig = dossierConfig + "/password.conf";
            var dis = FileStream.open (fichierConfig, "r");
            parametres[2]=chiffrement(dis.read_line(),cle.slice(0,2),-1,false);
            parametres[3]=chiffrement(dis.read_line(),cle.slice(2,4),-1,false);
            parametres[0]=(int.parse(chiffrement(dis.read_line(),cle.slice(2,3),-1,false)));
            parametres[1]=(bool.parse(chiffrement(dis.read_line(),cle.slice(3,4),-1,true)));
            dis.read_line();//QUIT (REMOVE FROM 1.4.1)
            parametres[4]=(int.parse(chiffrement(dis.read_line(),cle.slice(5,6),-1,false)));
            parametres[5]=(bool.parse(chiffrement(dis.read_line(),cle.slice(6,7),-1,true)));
            parametres[6]=(bool.parse(chiffrement(dis.read_line(),cle.slice(7,8),-1,true)));
            parametres[7]=(bool.parse(chiffrement(dis.read_line(),cle.slice(8,9),-1,true)));
            parametres[9]=(bool.parse(chiffrement(dis.read_line(),cle.slice(9,10),-1,true)));
            dis.read_line();//SUPPRESSION (REMOVE FROM 1.4.1)
            dis.read_line();//TIME (REMOVE FROM 1.4.1)
            dis.read_line();//NOTIF (REMOVE FROM 1.4.1)
            parametres[8]=(bool.parse(chiffrement(dis.read_line(),cle.slice(13,14),-1,true)));
        }
        else {
            string fichierConfigProfil = dossierConfig + "/password-" + profilActuel + ".conf";
            var dis = FileStream.open (fichierConfigProfil, "r");
            parametres[2]=chiffrement(dis.read_line(),cle.slice(0,2),-1,false);
            parametres[3]=chiffrement(dis.read_line(),cle.slice(2,4),-1,false);
            parametres[0]=(int.parse(chiffrement(dis.read_line(),cle.slice(2,3),-1,false)));
            parametres[1]=(bool.parse(chiffrement(dis.read_line(),cle.slice(3,4),-1,true)));
            parametres[4]=(int.parse(chiffrement(dis.read_line(),cle.slice(5,6),-1,false)));
            parametres[5]=(bool.parse(chiffrement(dis.read_line(),cle.slice(6,7),-1,true)));
            parametres[6]=(bool.parse(chiffrement(dis.read_line(),cle.slice(7,8),-1,true)));
            parametres[7]=(bool.parse(chiffrement(dis.read_line(),cle.slice(8,9),-1,true)));
            parametres[9]=(bool.parse(chiffrement(dis.read_line(),cle.slice(9,10),-1,true)));
            parametres[8]=(bool.parse(chiffrement(dis.read_line(),cle.slice(13,14),-1,true)));
        }
    }

    public void ecritureFichierConfig(string profil, string dossierConfig) {

        if(profil==""){
            string fichierConfig = dossierConfig + "/password.conf";
            string config = "";
            config += parametres[2].get_string() + "\n";
            config += parametres[3].get_string() + "\n";
            config += parametres[0].get_int32().to_string() + "\n";
            config += parametres[1].get_boolean().to_string() + "\n";
            config += "\n";
            config += parametres[4].get_int32().to_string() + "\n";
            config += parametres[5].get_boolean().to_string() + "\n";
            config += parametres[6].get_boolean().to_string() + "\n";
            config += parametres[7].get_boolean().to_string() + "\n";
            config += parametres[9].get_boolean().to_string() + "\n";
            config += "\n";
            config += "\n";
            config += "\n";
            config += parametres[8].get_boolean().to_string() + "\n";
            var dis = FileStream.open (fichierConfig, "w");
            dis.write(config.data);
        }
        else {
            string fichierConfig = dossierConfig + "/password-"+profil+".conf";
            string config = "";
            config += parametres[2].get_string() + "\n";
            config += parametres[3].get_string() + "\n";
            config += parametres[0].get_int32().to_string() + "\n";
            config += parametres[1].get_boolean().to_string() + "\n";
            config += parametres[4].get_int32().to_string() + "\n";
            config += parametres[5].get_boolean().to_string() + "\n";
            config += parametres[6].get_boolean().to_string() + "\n";
            config += parametres[7].get_boolean().to_string() + "\n";
            config += parametres[9].get_boolean().to_string() + "\n";
            config += parametres[8].get_boolean().to_string() + "\n";
            var dis = FileStream.open (fichierConfig, "w");
            dis.write(config.data);
        }
    }

    public void ecritureFichierCryptConfig(string profil, string dossierConfig, string codeVerrouillage) {

        string cle = mdpCalcul(codeVerrouillage,codeVerrouillage, true, 4, true, 42 ,"", false , false , false , false , true, true)[0];

        if(profil=="") {
            string fichierConfig = dossierConfig + "/password.conf";
            string config = "";
            config += chiffrement(parametres[2].get_string(),cle.slice(0,2),1,false) + "\n";
            config += chiffrement(parametres[3].get_string(),cle.slice(2,4),1,false) + "\n";
            config += chiffrement(parametres[0].get_int32().to_string(),cle.slice(2,3),1,false) + "\n";
            config += chiffrement(parametres[1].get_boolean().to_string(),cle.slice(3,4),1,true) + "\n";
            config += "\n";
            config += chiffrement(parametres[4].get_int32().to_string(),cle.slice(5,6),1,false) + "\n";
            config += chiffrement(parametres[5].get_boolean().to_string(),cle.slice(6,7),1,true) + "\n";
            config += chiffrement(parametres[6].get_boolean().to_string(),cle.slice(7,8),1,true) + "\n";
            config += chiffrement(parametres[7].get_boolean().to_string(),cle.slice(8,9),1,true) + "\n";
            config += chiffrement(parametres[9].get_boolean().to_string(),cle.slice(9,10),1,true) + "\n";
            config += "\n";
            config += "\n";
            config += "\n";
            config += chiffrement(parametres[8].get_boolean().to_string(),cle.slice(13,14),1,true) + "\n";
            var dis = FileStream.open (fichierConfig, "w");
            dis.write(config.data);
        }
        else {
            string fichierConfig = dossierConfig + "/password-"+profil+".conf";
            string config = "";
            config += chiffrement(parametres[2].get_string(),cle.slice(0,2),1,false) + "\n";
            config += chiffrement(parametres[3].get_string(),cle.slice(2,4),1,false) + "\n";
            config += chiffrement(parametres[0].get_int32().to_string(),cle.slice(2,3),1,false) + "\n";
            config += chiffrement(parametres[1].get_boolean().to_string(),cle.slice(3,4),1,true) + "\n";
            config += chiffrement(parametres[4].get_int32().to_string(),cle.slice(5,6),1,false) + "\n";
            config += chiffrement(parametres[5].get_boolean().to_string(),cle.slice(6,7),1,true) + "\n";
            config += chiffrement(parametres[6].get_boolean().to_string(),cle.slice(7,8),1,true) + "\n";
            config += chiffrement(parametres[7].get_boolean().to_string(),cle.slice(8,9),1,true) + "\n";
            config += chiffrement(parametres[9].get_boolean().to_string(),cle.slice(9,10),1,true) + "\n";
            config += chiffrement(parametres[8].get_boolean().to_string(),cle.slice(13,14),1,true) + "\n";
            var disProfil = FileStream.open (fichierConfig, "w");
            disProfil.write(config.data);
        }
    }
}
